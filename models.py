from flask_sqlalchemy import SQLAlchemy
from sqlalchemy import Column, Integer, String,Text,ForeignKey
from sqlalchemy.ext.declarative import declarative_base

db = SQLAlchemy()

class BaseModel(db.Model):
    """Base data model for all objects"""
    __abstract__ = True

    def __init__(self, *args):
        super().__init__(*args)

    def __repr__(self):
        """Define a base way to print models"""
        return '%s(%s)' % (self.__class__.__name__, self.__dict__)

    def json(self):
        """
                Define a base way to jsonify models, dealing with datetime objects
        """
        return self.__dict__

class Suppliers(BaseModel, db.Model):
    """Model for the Suppliers table"""
    __tablename__ = 'suppliers' 

    supplier_id = db.Column(db.Integer, primary_key = True)
    company_name = db.Column(db.String(40))
    contact_name = db.Column(db.String(30))
    contact_title = db.Column(db.String(30))
    address = db.Column(db.String(60))
    city = db.Column(db.String(15))
    region = db.Column(db.String(15))
    postal_code = db.Column(db.String(10))
    country = db.Column(db.String(15))
    phone = db.Column(db.String(24))
    fax = db.Column(db.String(24))
    homepage = db.Column(db.Text)


    
class Product(BaseModel, db.Model):
    """Model for the product table"""
    __tablename__ = 'product'

    product_id = db.Column(db.Integer, primary_key = True)
    product_name = db.Column(db.String(40))
    supplier_id = db.Column(db.Integer, ForeignKey('Suppliers.supplier_id'))
    categoty_id = db.Column(db.Integer)
    quantity_per_unit = db.Column(db.String(20))
    unit_price = db.Column(db.Integer)
    units_in_stock = db.Column(db.Integer)
    units_on_order = db.Column(db.Integer)
    reorder_lever = db.Column(db.Integer)
    discontinued = db.Column(db.Boolean)


