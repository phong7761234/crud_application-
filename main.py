import logging as logger
from flask import Flask, render_template, redirect, url_for, request, jsonify
from pip import __main__
from models import db,BaseModel,Suppliers,Product
import json

app = Flask(__name__)

POSTGRES = {
    'user': 'postgres',
    'pw': 'postgres',
    'db': 'data',
    'host': 'localhost',
    'port': '5432', 
}
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = True
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://%(user)s:\
%(pw)s@%(host)s:%(port)s/%(db)s' % POSTGRES
db.init_app(app)

def get_suppliers():
    return db.select([Suppliers])
def get_method(supplier_id = None):
    if request.method == 'GET':
        args = request.args.to_dict()
        # logger.warning("DELETE DONE %r ", args['supplier_id']

def post_method():
    if request.method == 'POST':
        logger.warning("METHOD POST")
        company_name = request.form['company_name']
        contact_name = request.form['contact_title']
        address = request.form['address']
        city = request.form['city']
        region = request.form['region']


@app.route("/", methods = ['GET', 'POST','DELETE'])
def hello(supplier_id = None):   
    get_method(supplier_id)
    post_method()
    list_supplier = get_suppliers()
    return render_template('product.html', data = list_supplier)

@app.route("/add/",methods=['GET', 'POST'])
@app.route("/update/<supplier_id>", methods=['GET', 'POST'])
def update(supplier_id = None):
    supplier = None
    list_supplier = get_suppliers()
    if supplier_id != None:
        for item in list_supplier:
            if item.supplier_id == int(supplier_id):
                supplier = item
    return render_template('update.html', data = supplier)

if __name__ == "__main__":
    app.run(debug=True)
